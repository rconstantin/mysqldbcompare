package fr.pentalog.hybris;

public class Field {

    private static final String ADD_COLUMN = "ADD COLUMN";
    private static final String CHANGE_COLUMN = "CHANGE COLUMN";
    private static final String MODIFY_COLUMN = "MODIFY COLUMN";

    private String name;
    private String type;
    private boolean nullable;
    private String defaultValue;

    public Field(String name, String type, boolean nullable, String defaultValue) {
        this.name = name;
        this.type = type;
        this.nullable = nullable;
        this.defaultValue = defaultValue;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public boolean isNullable() {
        return nullable;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public String toString() {
        return "Field [name=" + name + ", type=" + type + ", nullable=" + nullable + ", defaultValue=" + defaultValue + "]";
    }

    public String getAddColumnScriptFragment() {

        StringBuilder addColumnScript = new StringBuilder();

        addColumnScript.append(ADD_COLUMN).append(" ").append(getName()).append(" ").append(getType()).append(" ").append(isNullable() ? "NULL" : "NOT NULL");

        if (null != getDefaultValue() && !getDefaultValue().trim().isEmpty()) {
            addColumnScript.append(" DEFAULT ").append(getDefaultValue());
        }

        return addColumnScript.toString();
    }

    public String getChangeColumnScriptFragment(Field field) {

        StringBuilder addColumnScript = new StringBuilder();

        addColumnScript.append(CHANGE_COLUMN).append(" ").append(field.getName()).append(" ").append(getName());

        addColumnScript.append(" ").append(getType());
        addColumnScript.append(" ").append(isNullable() ? "NULL" : "NOT NULL");

        if (null != getDefaultValue() && !getDefaultValue().trim().isEmpty()) {
            addColumnScript.append(" DEFAULT ").append(getDefaultValue());
        }

        return addColumnScript.toString();
    }

    private boolean areEquals(String value1, String value2) {

        if (null == value1) {
            return null == value2;
        }

        return value1.equals(value2);
    }

    public String getModifyColumnScriptFragment(Field field) {

        if (getType().equals(field.getType()) && (isNullable() == field.isNullable()) && areEquals(getDefaultValue(), field.getDefaultValue())) {
            return null;
        }
        
        StringBuilder addColumnScript = new StringBuilder();

        addColumnScript.append(MODIFY_COLUMN).append(" ").append(getName());

        addColumnScript.append(" ").append(getType());
        addColumnScript.append(" ").append(isNullable() ? "NULL" : "NOT NULL");

        if (null != getDefaultValue() && !getDefaultValue().trim().isEmpty()) {
            addColumnScript.append(" DEFAULT ").append(getDefaultValue());
        }

        return addColumnScript.toString();
    }

}
