package fr.pentalog.hybris;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Database implements Closeable {
    private String host;
    private String account;
    private String password;
    private Connection connection;
    private SortedMap<String, Table> tables;
    private String database;

    public Database(String host, String database, String account, String password) throws SQLException {
        this.host = host;
        this.database = database;
        this.account = account;
        this.password = password;
        this.tables = new TreeMap<>();

        openConnection();
        parseTables();
    }

    private void openConnection() throws SQLException {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setServerName(host);
        dataSource.setDatabaseName(database);

        connection = dataSource.getConnection(account, password);
    }

    @Override
    public void close() throws IOException {
        if (null == connection) {
            return;
        }

        try {
            if (!connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new IOException(e);
        }
        connection = null;
    }

    private void parseTables() throws SQLException {
        String sql = "SHOW TABLES";

        try (PreparedStatement stmt = connection.prepareStatement(sql)) {

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String tableName = rs.getString(1);

                    Table table = new Table(tableName, connection);
                    tables.put(tableName, table);
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        this.tables.forEach((tableName, table) -> {
            sb.append("\n").append(table);
        });

        return sb.toString();
    }

    public Map<String, Table> getTables() {
        return tables;
    }

    private Map<String, String> getCreateTableScripts(Collection<String> tableNames) {
        return tableNames.stream().filter(tableName -> (null != getTables().get(tableName))).map(tableName -> tables.get(tableName))
                .collect(Collectors.toMap(Table::getName, Table::getCreateTableScript));

    }

    private Map<String, String> getCreateTableScriptsComparedWith(Database db2) {
        Set<String> newTables = this.getTables().keySet().stream().filter(tableName -> (null == db2.getTables().get(tableName))).collect(Collectors.toSet());

        return getCreateTableScripts(newTables);
    }

    private Map<String, String> computeAlterTableScripts(boolean dropedColumns, Database oldDB, Map<String, Map<String, String>> fieldMappings) {
        Set<String> sameTables = oldDB.getTables().keySet().stream().filter(tableName -> (null != Database.this.getTables().get(tableName)))
                .collect(Collectors.toSet());

        Map<String, String> changedAndNewColumns = sameTables.stream().map(tableName -> {

            Map<String, String> fieldsMappingPerTable = fieldMappings.get(tableName);
            if (null == fieldsMappingPerTable) {
                fieldsMappingPerTable = Collections.emptyMap();
            }

            Table newDbTable = Database.this.getTables().get(tableName);
            Table oldDbTable = oldDB.getTables().get(tableName);

            String alterTableScript;

            if (dropedColumns) {
                alterTableScript = newDbTable.getAlterTableScriptsDropFields(oldDbTable, fieldsMappingPerTable);
            } else {
                alterTableScript = newDbTable.getAlterTableScriptsChangedAndNewColumns(oldDbTable, fieldsMappingPerTable);
            }

            return new String[] { tableName, alterTableScript };

        }).filter(array -> (null != array[1])).collect(Collectors.toMap(array -> array[0], array -> array[1]));

        return changedAndNewColumns;
    }

    public boolean areMissingTables(Database oldDB) {
        // checking tables that need to be removed:
        Map<String, String> oldCreateTableScripts = oldDB.getCreateTableScriptsComparedWith(this);

        if (!oldCreateTableScripts.isEmpty()) {
            System.err.println("Some tables have been removed: " + oldCreateTableScripts.keySet());
            return true;
        }
        
        return false;
    }

    public Map<String, String> getNewAndChangedTables(Database oldDB, Map<String, Map<String, String>> fieldMappings) {
        // generating SQL scripts for new tables:
        Map<String, String> createTableScripts = getCreateTableScriptsComparedWith(oldDB);

        // generating SQL scripts for altering tables:
        Map<String, String> alterTableScripts = computeAlterTableScripts(false, oldDB, fieldMappings);

        Map<String, String> scripts = new LinkedHashMap<>();

        scripts.putAll(createTableScripts);
        scripts.putAll(alterTableScripts);

        return scripts;

    }

    public Map<String, String> getDropedColumns(Database oldDB, Map<String, Map<String, String>> fieldMappings) {
        // generating SQL scripts for altering tables:
        Map<String, String> alterTableScripts = computeAlterTableScripts(true, oldDB, fieldMappings);

        return alterTableScripts;
    }

}
