package fr.pentalog.hybris;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {

	private final static String OLDDB = "oldDb";
	private final static String NEWDB = "newDb";

	public static void main(String[] args) throws SQLException, IOException {

		if (args.length < 1) {
			System.err.println("Missing JSON mapping file. Pass it as the 1st paramater.");
			return;
		}

		Properties properties = ApplicationProperties.loadProperties("application.properties");
		DatabaseConnection oldDbConnection = new DatabaseConnection(OLDDB, properties);
		DatabaseConnection newDbConnection = new DatabaseConnection(NEWDB, properties);

		try (Database oldDB = new Database(oldDbConnection.getDbServer(), oldDbConnection.getDbName(),
				oldDbConnection.getDbUsername(), oldDbConnection.getDbPassword())) {

			try (Database newDB = new Database(newDbConnection.getDbServer(), newDbConnection.getDbName(),
					newDbConnection.getDbUsername(), newDbConnection.getDbPassword())) {

				final Map<String, Map<String, String>> fieldMappings = parseMappingJson(
						JsonUtil.differencesInTables(oldDB, newDB));

				String sqlScripts = compareDatabases(oldDB, newDB, fieldMappings,
						Boolean.valueOf(properties.getProperty("missingTablesAllowed", "false")));

				// System.err.println(sqlScripts);
			}
		}

	}

	private static Map<String, Map<String, String>> parseMappingJson(String jsonMappingContent)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.readValue(jsonMappingContent, new TypeReference<Map<String, Map<String, String>>>() {
		});
	}

	private static String compareDatabases(Database oldDB, Database newDB,
			Map<String, Map<String, String>> fieldMappings, Boolean missingTablesAllowed) {

		if (newDB.areMissingTables(oldDB) && !missingTablesAllowed) {
			return null;
		}

		Map<String, String> newAndChangedTables = newDB.getNewAndChangedTables(oldDB, fieldMappings);

		Map<String, String> dropedColumns = newDB.getDropedColumns(oldDB, fieldMappings);

		dropedColumns.entrySet().forEach(entry -> {
			System.err.println(String.format("Table: %s - %s", entry.getKey(), entry.getValue()));
		});

		Map<String, String> scripts = new LinkedHashMap<>();

		scripts.putAll(newAndChangedTables);
		scripts.putAll(dropedColumns);

		StringBuilder sqlScripts = scripts.values().stream().map(sql -> String.format("%s;\n", sql))
				.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append);

		return sqlScripts.toString();
	}

}
