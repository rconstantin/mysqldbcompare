package fr.pentalog.hybris;

import java.util.Properties;

public class DatabaseConnection {

	private final static String SERVER = ".server";
	private final static String NAME = ".name";
	private final static String USERNAME = ".username";
	private final static String PASSWORD = ".password";

	private String dbServer;
	private String dbName;
	private String dbUsername;
	private String dbPassword;

	public DatabaseConnection(String prefix, Properties properties) {
		this.dbServer = properties.getProperty(prefix + SERVER);
		this.dbName = properties.getProperty(prefix + NAME);
		this.dbUsername = properties.getProperty(prefix + USERNAME);
		this.dbPassword = properties.getProperty(prefix + PASSWORD);
	}

	public String getDbServer() {
		return dbServer;
	}

	public String getDbName() {
		return dbName;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public String getDbPassword() {
		return dbPassword;
	}

}
