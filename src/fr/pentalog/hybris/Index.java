package fr.pentalog.hybris;

public class Index {
    private String keyName;
    private int sequenceInIndex;
    private String fieldName;
    private String collation;
    private Integer subPart;
    private boolean nullable;

    public Index(String keyName, int sequenceInIndex, String fieldName, String collation, Integer subPart, boolean nullable) {
        super();
        this.keyName = keyName;
        this.sequenceInIndex = sequenceInIndex;
        this.fieldName = fieldName;
        this.collation = collation;
        this.subPart = subPart;
        this.nullable = nullable;
    }
}
