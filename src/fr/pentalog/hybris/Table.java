package fr.pentalog.hybris;

import java.lang.ref.WeakReference;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Table {
    private String tableName;
    private SortedMap<String, Field> tableFields;

    private WeakReference<Connection> connection;

    public Table(String name, Connection connection) throws SQLException {
        this.tableName = name;
        this.connection = new WeakReference<Connection>(connection);
        this.tableFields = new TreeMap<>();

        parseFields();
        parseIndexes();
    }

    private void parseFields() throws SQLException {
        String sql = String.format("show columns from `%s`", tableName);

        try (PreparedStatement stmt = connection.get().prepareStatement(sql)) {

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String fieldName = rs.getString("Field");
                    String fieldType = rs.getString("Type");
                    boolean nullableField = "YES".equals(rs.getString("Null"));
                    String defaultValue = rs.getString("Default");

                    Field field = new Field(fieldName, fieldType, nullableField, defaultValue);

                    tableFields.put(fieldName, field);
                }
            }
        }
    }

    private void parseIndexes() throws SQLException {
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n").append(tableName);

        this.tableFields.forEach((fieldName, field) -> {
            sb.append("\n\t").append(field);
        });

        return sb.toString();
    }

    public String getCreateTableScript() {
        String sql = String.format("SHOW CREATE TABLE `%s`", tableName);

        try (PreparedStatement stmt = connection.get().prepareStatement(sql)) {

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    return rs.getString(2);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        throw new IllegalStateException("Should not happen!");
    }

    public String getName() {
        return tableName;
    }

    private String toNewFieldNamePattern(String name) {
        return "p_" + name;
    }

    private static String toOldFieldNamePattern(String name) {
        if (name.startsWith("p_")) {
            return name.substring(2);
        }

        return name;
    }

    public String getAlterTableScriptsChangedAndNewColumns(Table oldDbTable, Map<String, String> fieldMappings) {

        Map<String, String> reversedFieldMappings = fieldMappings.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));

        Set<String> sameFields = getFields().keySet().stream().filter(fieldName -> {
            if (null == oldDbTable.getFields().get(fieldName)) {
                return false;
            }
            return true;
        }).collect(Collectors.toSet());

        Set<String> newFields = getFields().keySet().stream().filter(fieldName -> {
            if (null != oldDbTable.getFields().get(fieldName)) {
                return false;
            }
            if (null != oldDbTable.getFields().get(toOldFieldNamePattern(fieldName))) {
                return false;
            }
            if (null != reversedFieldMappings.get(fieldName)) {
                return false;
            }
            return true;
        }).collect(Collectors.toSet());

        Set<String> renamedFields = oldDbTable.getFields().keySet().stream().filter(fieldName -> {
            if (null != Table.this.getFields().get(fieldName)) {
                return false;
            }
            if (null != Table.this.getFields().get(toNewFieldNamePattern(fieldName))) {
                return false;
            }
            if (null != fieldMappings.get(fieldName)) {
                return true;
            }
            return false;
        }).collect(Collectors.toSet());

        Set<String> pRenamedFields = oldDbTable.getFields().keySet().stream().filter(fieldName -> {
            if (null != Table.this.getFields().get(fieldName)) {
                return false;
            }
            if (null == Table.this.getFields().get(toNewFieldNamePattern(fieldName))) {
                return false;
            }
            return true;
        }).collect(Collectors.toSet());

        // generate scripts for adding new table column fields:
        List<String> alterTableAddedColumns = newFields.stream().map(fieldName -> Table.this.getFields().get(fieldName).getAddColumnScriptFragment())
                .collect(Collectors.toList());

        // generate scripts for p_renamed table column fields:
        List<String> alterTablePRenamedColumns = pRenamedFields
                .stream()
                .map(fieldName -> Table.this.getFields().get(toNewFieldNamePattern(fieldName))
                        .getChangeColumnScriptFragment(oldDbTable.getFields().get(fieldName))).collect(Collectors.toList());

        // generate scripts for table columns that changed their definition:
        List<String> alterTableChangedColumnsDefinition = sameFields.stream()
                .map(fieldName -> Table.this.getFields().get(fieldName).getModifyColumnScriptFragment(oldDbTable.getFields().get(fieldName)))
                .filter(script -> (null != script)).collect(Collectors.toList());

        // generate scripts for table columns that changed their name:
        List<String> alterTableRenamedColumns = renamedFields.stream().map(fieldName -> {

            Field renamedField = Table.this.getFields().get(fieldMappings.get(fieldName));

            if (null == renamedField) {
                throw new IllegalStateException(String.format("Cannot find a mapping field for %s.%s", Table.this.tableName, fieldName));
            }

            return renamedField.getChangeColumnScriptFragment(oldDbTable.getFields().get(fieldName));
        }).collect(Collectors.toList());

        // merge all scripts:
        List<String> fieldScripts = new ArrayList<>();
        fieldScripts.addAll(alterTableAddedColumns);
        fieldScripts.addAll(alterTablePRenamedColumns);
        fieldScripts.addAll(alterTableChangedColumnsDefinition);
        fieldScripts.addAll(alterTableRenamedColumns);

        StringBuilder sb = fieldScripts.stream().map(fieldScript -> "\n\t" + fieldScript)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append);

        if (sb.length() <= 0) {
            return null;
        }

        return String.format("ALTER TABLE %s %s", getName(), sb.toString());
    }

    public Map<String, Field> getFields() {
        return tableFields;
    }

    public String getAlterTableScriptsDropFields(Table oldDbTable, Map<String, String> fieldMappings) {

        Set<String> removedFields = oldDbTable.getFields().keySet().stream().filter(fieldName -> {
            if (null != Table.this.getFields().get(fieldName)) {
                return false;
            }
            if (null != Table.this.getFields().get(toNewFieldNamePattern(fieldName))) {
                return false;
            }
            if (null == fieldMappings.get(fieldName)) {
                return true;
            }
            return false;
        }).collect(Collectors.toSet());

        // generate scripts for table columns that need to be removed:
        List<String> alterTableDropColumns = removedFields.stream().map(fieldName -> {
            return "DROP COLUMN " + fieldName;
        }).collect(Collectors.toList());

        // merge all scripts:
        List<String> fieldScripts = new ArrayList<>();

        fieldScripts.addAll(alterTableDropColumns);

        StringBuilder sb = fieldScripts.stream().map(fieldScript -> "\n\t" + fieldScript)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append);

        if (sb.length() <= 0) {
            return null;
        }

        return String.format("ALTER TABLE %s %s", getName(), sb.toString());
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tableName == null) ? 0 : tableName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Table other = (Table) obj;
		if (tableName == null) {
			if (other.tableName != null)
				return false;
		} else if (!tableName.equals(other.tableName))
			return false;
		return true;
	}

}
