package fr.pentalog.hybris;

public class TableDifference {

	private String oldRowName;
	private String newRowName;

	public TableDifference(String oldRowName, String newRowName) {
		this.oldRowName = oldRowName;
		this.newRowName = newRowName;
	}

	public String getOldRowName() {
		return oldRowName;
	}

	public void setOldRowName(String oldRowName) {
		this.oldRowName = oldRowName;
	}

	public String getNewRowName() {
		return newRowName;
	}

	public void setNewRowName(String newRowName) {
		this.newRowName = newRowName;
	}

}
