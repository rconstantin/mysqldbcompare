package fr.pentalog.hybris;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import info.debatty.java.stringsimilarity.JaroWinkler;

/**
 * Checks the difference in two tables using the Jaro-Winkler similarity
 * algorithm. See: https://github.com/tdebatty/java-string-similarity .
 * 
 * @author rconstantin
 *
 */
public class JsonUtil {

	public static String differencesInTables(Database oldDatabase, Database newDatabase) throws IOException {

		Properties properties = ApplicationProperties.loadProperties("application.properties");

		JaroWinkler jw = new JaroWinkler();

		SortedMap<String, Map<String, String>> differences = new TreeMap<>();

		newDatabase.getTables().values().retainAll(oldDatabase.getTables().values());

		Iterator<Table> oldTableIterator = oldDatabase.getTables().values().iterator();
		Iterator<Table> newTableIterator = newDatabase.getTables().values().iterator();
		while (oldTableIterator.hasNext()) {
			Table oldTable = oldTableIterator.next();
			Table newTable = newTableIterator.next();

			Map<String, Field> oldFields = oldTable.getFields();
			Map<String, Field> newFields = newTable.getFields();

			Iterator<Field> oldFieldIterator = oldFields.values().iterator();

			while (oldFieldIterator.hasNext()) {

				boolean shouldContinue = true;

				// We first check if the column exists, but at a different
				// position.
				Field oldField = oldFieldIterator.next();
				Iterator<Field> newFieldIterator = newFields.values().iterator();
				while (newFieldIterator.hasNext() && shouldContinue) {
					Field newField = newFieldIterator.next();

					if (oldField.getName().equals(newField.getName())) {
						shouldContinue = false;
						continue;
					}

				}

				// If we couldn't find it, we continue, checking the similarity
				// between the table names.
				if (shouldContinue) {
					newFieldIterator = newFields.values().iterator();
					while (newFieldIterator.hasNext() && shouldContinue) {
						Field newField = newFieldIterator.next();

						if (jw.similarity(oldField.getName(), newField.getName()) > Double
								.parseDouble(properties.getProperty("similarityMin"))
								&& !oldField.getName().startsWith("p_")
								&& !(oldField.getName().contains(newField.getName())
										|| (newField.getName().contains(oldField.getName())
												&& !newField.getName().startsWith("p_")))) {

							// We found a similarity!
							Map<String, String> oldDifferences = differences.get(newTable.getName());
							if (oldDifferences == null) {
								oldDifferences = new HashMap<>();
								oldDifferences.put(oldField.getName(), newField.getName());
							} else {
								if (oldDifferences.get(oldField.getName()) == null) {
									oldDifferences.put(oldField.getName(), newField.getName());
								} else {
									// This is a fallback in case we found
									// another similarity. If the similarity is
									// bigger, we put the new value.
									if (jw.similarity(oldField.getName(), newField.getName()) > jw
											.similarity(oldField.getName(), oldDifferences.get(oldField.getName()))) {
										oldDifferences.put(oldField.getName(), newField.getName());
									}
								}

							}
							differences.put(newTable.getName(), oldDifferences);
						}
					}
				}
			}
		}

		ObjectMapper mapper = new ObjectMapper();

		return mapper.writeValueAsString(differences);
	}
}
