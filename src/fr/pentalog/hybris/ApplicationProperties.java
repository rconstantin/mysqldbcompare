package fr.pentalog.hybris;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationProperties {

	private ApplicationProperties() {

	}

	private static Properties properties;

	public static Properties loadProperties(String propertiesFileName) throws IOException {

		properties = new Properties();
		final InputStream stream = ApplicationProperties.class.getClassLoader().getResourceAsStream(propertiesFileName);
		properties.load(stream);
		stream.close();
		return properties;

	}

}
